﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task-01
{
    class Program
    {
        static void Main(string[] args)
        {
            var age = 0;
            String name;

            Console.WriteLine("What is your name?");    //User input for Name
            name = (Console.ReadLine());

            Console.WriteLine("What is your age?"); //User Input for Age
            age = int.Parse(Console.ReadLine());

            //First String
            Console.WriteLine($"Your Age is: {age} ");
            Console.WriteLine("Your name is: {0} ", name);

            //Second String
            Console.WriteLine("My name is " + name + " and I am " + age + " years old");

            //Third String
            Console.WriteLine("My name is {0} and I am {1} years old", name, age);

            Console.ReadLine();

        }

    }
}
