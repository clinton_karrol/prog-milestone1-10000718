﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in a number so that the program can determine where it is odd or even");
            int n = int.Parse(Console.ReadLine());

            bool result = n % 2 == 0;
            Console.WriteLine("Your number {0} is even? Answer: {1}", n, result);
            Console.ReadLine();

        }
    }
}
